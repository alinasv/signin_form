 Welcome to my SignIn project.

@Description: 

 The main purpose of this project is to show the short and simple registration from for new User. With fields validation its gets all needed infromation from the input and save it to DB. After transfer user to the new page where it is possible to see all the information user added and edit it if needed plus possible to add new information like
the phone numnber.  

@Version: 1.0.0

@Structure: 
 Project runs on local WAMP (Apache, MySQL, PHP) server, with using Smarty web template system to help devide Front End and Back End side of the project. 
The main working directories are BackEnd where located all .php files; FrontEnd where located all user interation parts: css, js directories; templates directory where saved all .tpl files with html code; db directory with file connection and interact with MySQL.

@Instalation guide: 
 To work with project localy you should have local WAMP server on your machine. 
The guidlines to install it you can find here: https://www.youtube.com/watch?v=yWstRAlAX1A&list=PLS1QulWo1RIZwGGYH4pm-WitfAUID_ZeR 
After it is installed you can save this project to you wamp/www/ folder. To be able run it you need to correct the paths so they lead to the correct files on your machine in file: registerFrorm.php, profile.php

@Demo: 
 The demo version is hosting on AWS, you can find it over the next link: http://18.220.86.126/SignIn_Form/libs/BackEnd/registerForm.php