<?php
/* Smarty version 3.1.31, created on 2018-11-07 13:40:41
  from "D:\Wamp\wamp\www\Register\libs\templates\registerForm.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5be2dd49f054f8_35257109',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3d6b871cbc0f6c494c66e89183f6943056c6af45' => 
    array (
      0 => 'D:\\Wamp\\wamp\\www\\Register\\libs\\templates\\registerForm.tpl',
      1 => 1541594262,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5be2dd49f054f8_35257109 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html>

	<head lang="en">
         <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
		
		 <!-- Bootstrap CSS-->	
		 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
               integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

		 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

        <!-- CSS main file -->

        <link rel="stylesheet" type="text/css"	href='../FrontEnd/styles/style.css'/>

		 <!-- Fonts -->	
		 <link href="https://fonts.googleapis.com/css?family=Overpass|Source+Sans+Pro" rel="stylesheet">

		 <!-- Icons from Font-Awesome -->
		 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

       
		 <title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</title>
	</head>
	

	
	<body>
		
		<div class = "header"> </div>
		
		<div class = "container" id="form-container">
		
			<div class = "row-content">
				<div id = "registerDiv">
					<div class = "col-12 col-md-9">
					   <h4 id = "rewRegForm"><?php echo $_smarty_tpl->tpl_vars['titleInvitation']->value;?>
</h4>
					</div>
				</div>
				<div class = "col-12 col-md-9"> 
					<form action = "registerForm.php" method = "POST" id="registerForm">
						<!--Email field -->
						<div class = "form-group">
							<label class = "labelText" class="requiredFields"><?php echo $_smarty_tpl->tpl_vars['emailLable']->value;?>
</label >
                            <input type = "text" placeholder="Please type your e-mail address" name = "uname" class="form-control">
						</div>  
						
						
						<!--DOB fields -->
						<div class = "form-group" id = "dobDisplay">
							<label class = "labelText" class="requiredFields"><?php echo $_smarty_tpl->tpl_vars['dobLable']->value;?>
</label>
							
		                 <div class = "form-group row">
						
							 <div class = "col-md-4">
							    <label class = "dobFields"><?php echo $_smarty_tpl->tpl_vars['ddlLable']->value;?>
</label>
				      				 <input type = "text" class="form-control" placeholder="DD" name = "dd">

							</div>
							
							<div class = "col-md-4">
							    <label class = "dobFields"><?php echo $_smarty_tpl->tpl_vars['mmLable']->value;?>
</label>
				       				<input type = "text" name = "mm" class="form-control" placeholder="MM" min="1" max="12">

							</div>
							
							<div class = "col-md-4">
							    <label class = "dobFields"><?php echo $_smarty_tpl->tpl_vars['yyLable']->value;?>
</label>
				      				 <input type = "text" name="yy" class="form-control" placeholder="YYYY" min="1901">

							</div> 
						 </div>							
							
						</div>
						
						<!--Password fields field -->
						
						<div class = "form-group">
							<label class = "labelText" class="requiredFields"><?php echo $_smarty_tpl->tpl_vars['passwordLable']->value;?>
</label >
							
							<input type = "password" name = "pass1" class="form-control" id = "pass1" placeholder="Password">
							
							<br/>
							
							<input type = "password" name = "pass2" class="form-control" placeholder="Please type in your pass again">
							
							
						</div>
						
						
							<!--First name field -->
						<div class = "form-group">
							<label class = "labelText" class="requiredFields"><?php echo $_smarty_tpl->tpl_vars['fnLable']->value;?>
</label >
							<div class = "">
								<input type = "text" name="fn" class = "form-control" placeholder="First name">
							</div>		
						</div>
						
						<!-- Last name field --> 
						<div class = "form-group">
							<label class = "labelText" class="requiredFields"><?php echo $_smarty_tpl->tpl_vars['lnLable']->value;?>
</label>
							<div class = "">
								<input type = "text" name="ln" class = "form-control" placeholder="Last name">
							</div>		
						</div>
						
						<div class = "form-group">
						    <label class = "labelText"><?php echo $_smarty_tpl->tpl_vars['genderLable']->value;?>
</label >
							
							
							<div class = "form-group row">
							
								<div class = "col-4">
									<button type="button" class="btn btn-default btn-circle btn-xl" id="genM">
										<h6 id = "genderDefMale"> <?php echo $_smarty_tpl->tpl_vars['male']->value;?>

											<input type="radio" name="gender" class="radioBtn" value="Male">
										</h6>
									</button>
							    </div>
								
								<div class = "col-4">

									<button type="button" class="btn btn-default btn-circle btn-xl" id="genF">
										<h6 id = "genderDefMale"> <?php echo $_smarty_tpl->tpl_vars['female']->value;?>

											<input type="radio" name="gender" class="radioBtn" value="Female">
										</h6>
									</button>
							    </div>	
							</div>
							
						</div>
						
						<div class = "form-group">
							<br/>
							<div class="form-check">
								
								<label class="form-check-label">
									<input type="checkbox" class="form-check-input" name="tAndc">
 
									<h6 id = "tAndc"><?php echo $_smarty_tpl->tpl_vars['termAndCond']->value;?>
 </h6>
								</label>
								
                           </div>
						</div>
								
						<div class="form-group">
							<button type="submit" class="btn btn-dark btn-lg btn-block" id="registerBtn">Register</button>
						</div>
					
					</form>
				</div>
			</div>
		
		</div>
		
		<div class="footer">
			<div></div>
		</div>

				<!-- JS file -->
		<?php echo '<script'; ?>
	type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"><?php echo '</script'; ?>
>
		
		<?php echo '<script'; ?>
 src = https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js><?php echo '</script'; ?>
>
		
		<?php echo '<script'; ?>
 type="text/javascript" src="../FrontEnd/js/mainJS.js"><?php echo '</script'; ?>
>
		
		<?php echo '<script'; ?>
 src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"><?php echo '</script'; ?>
>

	</body>
	
	
</html><?php }
}
