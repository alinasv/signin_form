<?php
/**
 * Created by PhpStorm.
 * User: Alina
 * Date: 22.10.2018
 * Time: 10:30
 */

namespace db;
require 'Connection.php';

  /*
    class which makes connection with DB and asign the
    appropriate table for future work
  */

class Object extends Connection{

    private $table;
    private $connection;

    public function __construct($table){
        $this->setTable($table);
        $this->connection = new Connection();
    }

    public function getTale(){
        return $this->table;
    }

    public function setTable($tableTitle){
        $this->table = '`register`.'.$tableTitle.'`';
    }

    public function getConnection(){
        return $this->connection;
    }

}