<?php
/**
 * Created by PhpStorm.
 * User: Alina
 * Date: 22.10.2018
 * Time: 9:46
 */

namespace db;

  /*
    class to create connection with DB
    and perform query to DB
  */

class Connection {

    private $connection;

    private $server_name;
    private $username;
    private $password;
    private $db_name;


    public function __construct() {
        $this->server_name = "localhost";
        $this->username = 'root';
        $this->password = 'Alina_DB';
        $this->db_name = 'register';


        $this->connection = new \mysqli($this->server_name, $this->username, $this->password, $this->db_name);
    }

    public function query($sql){
        return $this->connection->query($sql);
    }

}