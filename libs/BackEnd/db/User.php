<?php
/**
 * Created by PhpStorm.
 * User: Alina
 * Date: 22.10.2018
 * Time: 10:45
 */

namespace db;
require 'Object.php';

class User extends Object {

    public function __construct() {
        parent::__construct('users');
    }

    public function addUser($email, $password, $fn, $ln, $dob, $gender, $user_type = 'user'){
        $sql = "INSERT INTO `register`.`users` (`email`, `password`, `first_name`, `last_name`, `dob`, `gender`, `user_type`) VALUES('$email', MD5('$password'), '$fn', '$ln', '$dob', '$gender','$user_type')";
        parent::getConnection()->query($sql);
    }


    public function findUserByEmail($email){
        $sql = "SELECT id_user FROM register.users WHERE email = '$email';";
        return parent::getConnection()->query($sql);
    }


    public function updateUserEmail($data, $email){
        $sql = "UPDATE register.users SET email = '$data' WHERE email = '$email';";
        return parent::getConnection()->query($sql);
    }

    public function updateUserFN($data, $email){
        $sql = "UPDATE register.users SET first_name = '$data' WHERE email = '$email';";
        return parent::getConnection()->query($sql);
    }

    public function updateUserLN($data, $email){
        $sql = "UPDATE register.users SET last_name = '$data' WHERE email = '$email';";
        return parent::getConnection()->query($sql);
    }

    public function updateUserDOB($data, $email){
        $sql = "UPDATE register.users SET dob = '$data' WHERE email = '$email';";
        return parent::getConnection()->query($sql);
    }

    public function updateUserGender($data, $email){
        $sql = "UPDATE register.users SET gender = '$data' WHERE email = '$email';";
        return parent::getConnection()->query($sql);
    }
    public function updateUserPhone($data, $email){
        $sql = "UPDATE register.users SET phoneNum = '$data' WHERE email = '$email';";
        return parent::getConnection()->query($sql);
    }

    public function updateUserPassword($data, $email){
        $sql = "UPDATE register.users SET password = MD5('$data') WHERE email = '$email';";
        return parent::getConnection()->query($sql);
    }

    public function getUserFN($email){
        $sql = "SELECT first FROM register.users WHERE email = '$email';";
        return parent::getConnection()->query($sql);
    }
}

/*
$user = new User();
$email = 'hoeiejuytf@gmail.com';
$ps = 'Qwerty3214';
$fn = 'Alph';
$ln = 'Brooks';
$dob = '1990-12-12';
$gender = 'male';
$user_type = 'user';

$user->addUser($email, $ps, $fn, $ln, $dob, $gender, $user_type);


$user2 = new User();
$email = 'alphOps@gmail.com';

$idQ = $user2->findUserByEmail($email);

if($idQ -> num_rows > 0){
    while ($row = $idQ->fetch_assoc()){
        $id = $row['id_user'];
        echo "Id: ".$id;
    }
}
else {
    echo "No info";
}

$user = new User();
$phone = '980980980980';
$user->updateUserPhone($phone, 'patric.james@gmail.com');
*/