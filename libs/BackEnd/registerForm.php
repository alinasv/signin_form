<?php /** @noinspection ALL */

  require 'D:/Wamp/wamp/www/Register/libs/BackEnd/SmartyClasses/Smarty.class.php';
  require '\db\User.php';

  $smarty = new Smarty();

  $smarty->template_dir = "D:/Wamp/wamp/www/Register/libs/templates";
  $smarty->compile_dir = "D:/Wamp/wamp/www/Register/libs/templates_c";
  $smarty->cache_dir = "D:/Wamp/wamp/www/Register/libs/cache"; 
  $smarty->config_dir = "D:/Wamp/wamp/www/Register/libs/configs";   
  
  /* declare labels values */
  $title = "Register Form";

  $titleInvitation = "Sign up";
  $emailLable = "Your e-mail";
  $dobLable = "Your date of birth";
  $ddlLable = "Day";
  $mmLable = "Month";
  $yyLable = "Year";
  $passwordLable = "Choose password";
  $fnLable = "First name";
  $lnLable = "Last name";
  $genderLable = "Gender";
  $female = "Female";
  $male = "Male";
  $termAndCond = "I have read and accepted Terms and Conditions";

  /* assign labels values */
  $smarty->assign("title",$title);

  $smarty->assign("titleInvitation",$titleInvitation);
  $smarty->assign("emailLable",$emailLable);
  $smarty->assign("dobLable",$dobLable);
  $smarty->assign("ddlLable",$ddlLable);
  $smarty->assign("mmLable",$mmLable);
  $smarty->assign("mmLable",$mmLable);
  $smarty->assign("yyLable",$yyLable);
  $smarty->assign("passwordLable",$passwordLable);
  $smarty->assign("fnLable",$fnLable);
  $smarty->assign("lnLable",$lnLable);
  $smarty->assign("genderLable",$genderLable);
  $smarty->assign("female",$female);
  $smarty->assign("male",$male);
  $smarty->assign("termAndCond",$termAndCond);



if($_SERVER['REQUEST_METHOD'] === 'POST'){

    session_start();

    $user = new \db\User();

    //check if user with e-mail exists:
    $email = $_POST['uname'];
    $ps = $_POST['pass1'];
    $fn = $_POST['fn'];
    $ln = $_POST['ln'];
    $gender = "Other";
    if(!empty($_POST["gender"])){
        $gender = $_POST["gender"];
    }

    $user_type = 'user';

    /*dob*/
    $dd = $_POST['dd'];
    $mm = $_POST['mm'];
    $yy = $_POST['yy'];

    $dob = $yy.'-'.$mm.'-'.$dd;

    //check if user with this $email exists:
    $idQ = $user->findUserByEmail($email);
    if($idQ -> num_rows > 0){
       while($row = $idQ -> fetch_assoc()){
           $id = $row['id_user'];
           echo "Such user already exsists";
       }
    }

    //if no such user -> add it;
    else {
        $user->addUser($email, $ps, $fn, $ln, $dob, $gender, $user_type);

        // check again if query proceeded and if it is
        // refer to the Profile page
        $idQ2 = $user->findUserByEmail($email);
        if($idQ2 -> num_rows > 0){

            //add variables to Session:
            $_SESSION["email"] = $email;
            $_SESSION["pass"] = $ps;
            $_SESSION["fn"] = $fn;
            $_SESSION["ln"] = $ln;
            $_SESSION["gender"] = $gender;
            $_SESSION["dd"] = $dd;
            $_SESSION["mm"] = $mm;
            $_SESSION["yy"] = $yy;

            header("Location: profile.php");
        }
        else {
            echo "Some erros appeared";
        }
    }
}

$smarty->display("registerForm.tpl");