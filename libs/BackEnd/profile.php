<?php

    require 'D:/Wamp/wamp/www/Register/libs/BackEnd/SmartyClasses/Smarty.class.php';
    require '.\db\User.php';

    $smarty = new Smarty();

    $smarty->template_dir = "D:/Wamp/wamp/www/Register/libs/templates";
    $smarty->compile_dir = "D:/Wamp/wamp/www/Register/libs/templates_c";
    $smarty->cache_dir = "D:/Wamp/wamp/www/Register/libs/cache";
    $smarty->config_dir = "D:/Wamp/wamp/www/Register/libs/configs";


      $title = "Profile";
      $smarty->assign("title", $title);
     session_start();
      /*
        create associative array to save user data
      */
      $data = array();
      $data["fn"] = $_SESSION["fn"];
      $data["ln"] = $_SESSION["ln"];
      $data['email'] = $_SESSION["email"];
      $data['pass'] = $_SESSION["pass"];
      $data['gender'] = $_SESSION["gender"];
      $data['phone'] = "";

      $data['dd'] = $_SESSION["dd"];
      $data['mm'] = $_SESSION["mm"];
      $data['yy'] = $_SESSION["yy"];
      $data['dob'] = "";

      //DOB
      $dd = $_SESSION["dd"];
      $mm = $_SESSION["mm"];
      $yy = $_SESSION["yy"];

      /*
        Get and send user data to be displayed;
      */
      if(!empty($_POST["req"])){
          echo json_encode($data);
          die();
      }

      /*
        data taken from user input which was sent via ajax from  mainJS
        and changes sent to be updated in DB
      */
      if(!empty($_POST["t"])){
          $ttl = $_POST["t"];
          $val = $_POST["v"];
          $id = $_POST["i"];

          $user = new \db\User();

          if($ttl === "First Name"){
              $user->updateUserFN($val, $id);
              $data["fn"] = $val;
              echo json_encode($data);
              die();
          }

          else if($ttl === "Last Name"){
              $user->updateUserLN($val, $id);
              $data["ln"] = $val;
              echo json_encode($data);
              die();
          }

          else if($ttl === "Email"){
              $user->updateUserEmail($val, $id);
              $data["email"] = $val;
              echo json_encode($data);
              die();
          }

          else if($ttl === "Date Of Birth"){
              $user->updateUserDOB($val, $id);
              $data["dob"] = $val;
              echo json_encode($data);
              die();
          }

          else if($ttl === "Gender"){
              $user->updateUserGender($val, $id);
              $data["gender"] = $val;
              echo json_encode($data);
              die();
          }

          else if($ttl === "Phone number"){
              $user->updateUserPhone($val, $id);
              $data["phone"] = $val;
              echo json_encode($data);
              die();
          }
          else if($ttl === "Password"){
              $user->updateUserPassword($val, $id);
              $data["pass"] = $val;
              echo json_encode($data);
              die();
          }
      }


$smarty->display("profile.tpl");