<!DOCTYPE html>
<html>

	<head lang="en">
		<meta http-equiv="Content-Type" content="text/html" charset="UTF-8">

		<!-- Bootstrap CSS-->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
			  integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

		<!-- CSS main file -->

		<link rel="stylesheet" type="text/css"	href='../FrontEnd/styles/style.css'/>

		<!-- Fonts -->
		<link href="https://fonts.googleapis.com/css?family=Overpass|Source+Sans+Pro" rel="stylesheet">

		<!-- Icons from Font-Awesome -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


		<title>{$title}</title>
	</head>
	
	
	<body>

		<div id = "editModal" class = "modal fade" role="dialog">
			<div class = "modal-dialog modal-sm" role="dialog">
				<div class = "modal-content">
					
					<div class = "modal-header">
						<h4 class = "modal-title">Change item</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
						<div class = "modal-body">
							<label for = "item" class = "control-label" id = "titleModal"></label>
							<input type = "text" name="item" id="textModal" class = "form-control">
						</div>

						<div class = "modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
							<button type="button" class="btn btn-primary" id="saveModalBtn">Save</button>
						</div>
				</div>
			</div>
		</div>

		<!-- create DOB modal -->
		<div id = "dobModal" class = "modal fade" role="dialog">
			<div class = "modal-dialog modal-sm" role="dialog">
				<div class = "modal-content">

					<div class = "modal-header">
						<h4 class = "modal-title">Change Date Of Birth</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>

					<div class = "modal-body">
						<label for = "dobItem" class = "control-label" id = "titleModalDay">Day</label>
						<input type = "text" name="dobItem" class = "form-control" id="textModalDay">
                              <br/>
						<label for = "dobItem" class = "control-label" id = "titleModalMonth">Month</label>
						<input type = "text" name="dobItem" class = "form-control" id="textModalMonth">
						      <br/>
						<label for = "dobItem" class = "control-label" id = "titleModalYear">Year</label>
						<input type = "text" name="dobItem" class = "form-control" id="textModalYear">
					</div>

					<div class = "modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
						<button type="button" class="btn btn-primary" id="saveDobBtn">Save</button>
					</div>
				</div>
			</div>
		</div>

		<div id = "editGenderModal" class = "modal fade" role="dialog">
			<div class = "modal-dialog modal-sm" role="dialog">
				<div class = "modal-content">

					<div class = "modal-header">
						<h4 class = "modal-title">Change gender</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<div class = "modal-body">
						<label for = "item" class = "control-label" id = "titleGenModal"></label>

						<select class = "form-control form-control-lg" name = "genderList" id = "modalList" data-style="btn-warning">
							<option id = "fOption">Female</option>
							<option id = "mOption">Male</option>
							<option id = "otherOption">Other</option>
						</select>
					</div>

					<div class = "modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
						<button type="button" class="btn btn-primary" id="saveModalGenderBtn">Save</button>
					</div>
				</div>
			</div>
		</div>

		<div  id = "passModal" class = "modal fade" role="dialog">
			<div class = "modal-dialog modal-sm" role="dialog">
				<div class = "modal-content">
					<div class = "modal-header">
						<h4 class = "modal-title">Change Password</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>

					<div class = "modal-body">

						<div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							Failed
						</div>

						<label for = "dobItem" class = "control-label" id = "oldPassModal">Old Password</label>
						<input type = "text" name="dobItem" class = "form-control" id="textOldPass">

						<br/>

						<label for = "dobItem" class = "control-label" id = "newPassModal">New Password</label>
						<input type = "text" name="dobItem" class = "form-control" id="textNewPass">
					</div>

					<div class = "modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
						<button type="button" class="btn btn-primary" id="savePassBtn">Save</button>
					</div>
				</div>
			</div>
		</div>

		<div class = "header"></div>

		<div class = "container" id="profileContainer">	
			<div class="row" id = "info">
				<div class = "col-12">
					<h1>Your personal information</h1>
					<hr/>
				</div>
			</div>
			
			<br/>
			
		</div>
		
		<div class="row Panel_template" id = "templatePanel">

			<div class = "item_template">


			 <div id = "itemTemplate" class = "col-10 offset-1">
				 <div class = "col-4" id = "title"></div>
				 <div class = "col-4" id = "item"></div>
				 <div class = "col-4">
					 <button type="button" class="btn btn-outline-primary" id = "editBtn">Edit</button>
				 </div>
			 </div>
			 
			 <div class = "col-10 offset-1" id = "editTemplate">
				 <div class = "col-4" id = "data"></div>
				 <div class = "col-4"></div>
				 <div class = "col-4 float-right">
				   <a data-toggle="modal" data-target="#editModal" href = "#editModal">
					   <i class="fa fa-pencil fa-fw" id="editPencil"></i>
				   </a>
				 </div>
				 
			 </div>
			</div>
		 </div>

		<!-- DOB panel -->
		<div class="row Panel_template" id = "templatePanel_dob">
			<div class = "item_template">

				<div id = "itemTemplate" class = "col-10 offset-1">
					<div class = "col-4" id = "title"></div>
					<div class = "col-4" id = "item"></div>
					<div class = "col-4">
						<button type="button" class="btn btn-outline-primary" id = "editBtn">Edit</button>
					</div>
				</div>

				<div class = "col-10 offset-1" id = "editTemplate">
					<div class = "col-4" id = "data"></div>
					<div class = "col-4"></div>
					<div class = "col-4 float-right">
						<a data-toggle="modal" data-target="#dobModal" href = "#dobModal">
							<i class="fa fa-pencil fa-fw" id="editPencil"></i>
						</a>
					</div>

				</div>
			</div>
		</div>

		<!-- Gender panel -->
		<div class="row Panel_template" id = "templatePanel_gender">
			<div class = "item_template">

				<div id = "itemTemplate" class = "col-10 offset-1">
					<div class = "col-4" id = "title"></div>
					<div class = "col-4" id = "item"></div>
					<div class = "col-4">
						<button type="button" class="btn btn-outline-primary" id = "editBtn">Edit</button>
					</div>
				</div>

				<div class = "col-10 offset-1" id = "editTemplate">
					<div class = "col-4" id = "data"></div>
					<div class = "col-4"></div>
					<div class = "col-4 float-right">
						<a data-toggle="modal" data-target="#editGenderModal" href = "#editGenderModal">
							<i class="fa fa-pencil fa-fw" id="editPencil"></i>
						</a>
					</div>

				</div>
			</div>
		</div>

		<!-- Pass panel -->
		<div class="row Panel_template" id = "templatePanel_pass">
			<div class = "item_template">

				<div id = "itemTemplate" class = "col-10 offset-1">
					<div class = "col-4" id = "title"></div>
					<div class = "col-4" id = "item"></div>
					<div class = "col-4">
						<button type="button" class="btn btn-outline-primary" id = "editBtn">Edit</button>
					</div>
				</div>

				<div class = "col-10 offset-1" id = "editTemplate">
					<div class = "col-4" id = "data"></div>
					<div class = "col-4"></div>
					<div class = "col-4 float-right">
						<a data-toggle="modal" data-target="#passModal" href = "#passModal">
							<i class="fa fa-pencil fa-fw" id="editPencil"></i>
						</a>
					</div>

				</div>
			</div>
		</div>



		<div class="footer"></div>

		<!-- JS file -->
		<script	type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>

		<script src = https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js></script>

		<script type="text/javascript" src="../FrontEnd/js/mainJS.js"></script>

		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"> </script>
		
	</body>
	
</html>