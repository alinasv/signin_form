$(function(){

	/*
	   Animation for gender buttons
	*/

	let $genM = $("#genM");
    let $genF = $("#genF");
	
	// Gender => male
	$genM.click(function(){
		$genF.css("background-color", "#fff");
		$(this).css("background-color", "#ef6c00");
	});
	
	// Gender => female
	$genF.click(function(){
		$genM.css("background-color", "#fff");
		$(this).css("background-color", "#ef6c00");
	});

	/*
	 ========================================================
	*/


	/*
	HTML data taken from each panel to create block with needed information;
	*/

    let TEMPLATE_PANEL = $("#templatePanel").html();
    let TEMPLATE_PANEL_DOB = $("#templatePanel_dob").html();
    let TEMPLATE_PANEL_PASS = $("#templatePanel_pass").html();
    let TEMPLATE_GENDER = $("#templatePanel_gender").html();

    let $list = $("#profileContainer");

    /*
	Data for first edit Modal: name, email, phone number;
	*/
    let $modalFNLabel = $("#titleModal");
    let $modalFN = $("#textModal");
    let $saveModalBtn = $("#saveModalBtn");

    /*
	Data for gender modification  Modal: name, email, phone number;
	*/
    let $modalGenLb = $("#titleGenModal");
    let $modalGenderFemale = $("#fOption");
    let $modalGenderMale = $("#mOption");
    let $modalGenderOther = $("#otherOption");
    let $saveGenBtn = $("#saveModalGenderBtn");

    /*
	 ========================================================
	*/


    /*
       addBlock method: gather all data from templatePanel to use it further:
       display panel with data, enale buttons, takes and insert data to DB via ajax calls;
    */

    /*
      function for name, email and phone number manage
     */
    function addBlock(title, text, idNum){
        /*
          assign new node;
         */
        let $node = $(TEMPLATE_PANEL);

        /*
          collect all needed used elements at node
         */
        let $title = $node.find("#title");
        let $item = $node.find("#item");
        let $data = $node.find("#data");


        let $editTemplate = $node.find("#editTemplate");
        let $editBtn = $node.find("#editBtn");

        let $editPencil = $node.find("#editPencil");

        $title.text(title);
        $item.text(text);
        $data.text(text);

        /*
          each node from a block should have separate class
          depending on its title
        */
        if(title === "First Name"){
            $item.addClass("firstItem");
            $data.addClass("firstData");
        }
        if(title === "Last Name"){
            $item.addClass("secondItem");
            $data.addClass("secondData");
        }

        if(title === "Email"){
            $item.addClass("thirdItem");
            $data.addClass("thirdData");
        }

        if(title === "Phone number"){
            $item.addClass("fourthItem");
            $data.addClass("fourthData");
        }

        /*
         adding node to the list be displayed with all its elements
         */
        $list.append($node);

        /*
         function to count clicks of the Edit button:
         on first click its displays edit template but in case user changes mind and do not wish
         edit anymore it's closed
        */
        $editBtn.click(function(){
            let click = $(this).data('click');

            if(click){
                $editTemplate.css("display", "flex");
                $editTemplate.slideDown();
            }
            else {
                $editTemplate.css("display", "none");
                $editTemplate.slideUp();
            }
            $(this).data("click", !click);

        });

        /*
          enable work after $editPencil clicked:
        * */
        $editPencil.click(function () {

            /*
             display current node title and user info on the modal
             */
            $modalFNLabel.text(title);
            let $info = $data.text();
            $modalFN.val($info);

            /*
              declare variable which will keep new data inserted by user
              on the appropriate modal window
             */
            let $updatedData;
            $saveModalBtn.click(function () {
                /*
                  get data from modal window;
                  send it with ajax to profile.php to be updated in DB
                  save the changes in the associative array $data and parse
                  needed data from the array according to the clicked node
                 */
                let $dataInput = $modalFN.val();
                $.ajax({
                    type: "POST",
                    url: "../BackEnd/profile.php",
                    dataType: "text",
                    data: {t: $modalFNLabel.text().toString(), i: idNum, v : $dataInput},
                    success: function (data) {
                        let tJson = JSON.parse(data);

                        /*
                          after needed data parsed get the needed element by key
                          and display changes;
                          additional hide modal and edit panel
                         */
                        if($modalFNLabel.text() === "First Name"){
                            $updatedData = (tJson["fn"]).toString();
                            $("#editModal").modal('hide');
                            $(".firstItem").text($updatedData);
                            $(".firstData").text($updatedData);
                            $editTemplate.hide();
                        }
                        else if ($modalFNLabel.text() === "Last Name") {
                            $updatedData = (tJson["ln"]).toString();
                            $("#editModal").modal('hide');
                            $(".secondItem").text($updatedData);
                            $(".secondData").text($updatedData);
                            $editTemplate.hide();
                        }
                        else if ($modalFNLabel.text() === "Email") {
                            $updatedData = (tJson["email"]).toString();
                            $("#editModal").modal('hide');
                            $(".thirdItem").text($updatedData);
                            $(".thirdData").text($updatedData);
                            $editTemplate.hide();
                        }
                         else if ($modalFNLabel.text() === "Phone number") {
                            $updatedData = (tJson["phone"]).toString();
                            $("#editModal").modal('hide');
                            $(".fourthItem").text($updatedData);
                            $(".fourthData").text($updatedData);
                            $editTemplate.hide();
                        }
                    }
                });
             });

        });
}


	/*function for dob manage*/

	function addDOB_Block(title, text, idNum){
        let $node = $(TEMPLATE_PANEL_DOB);

        let $title = $node.find("#title");
        let $item = $node.find("#item");
        let $data = $node.find("#data");


        let $editTemplate = $node.find("#editTemplate");
        let $editBtn = $node.find("#editBtn");

        $title.text(title);
        $item.text(text);
        $data.text(text);

        $list.append($node);

        /*Show edit element on first click and hide it on second*/
        $editBtn.click(function(){
            let click = $(this).data('click');

            if(click){
                $editTemplate.css("display", "flex");
                $editTemplate.slideDown();
            }
            else {
                $editTemplate.css("display", "none");
                $editTemplate.slideUp();
            }
            $(this).data("click", !click);
        });

           let $textModalDay = $("#textModalDay");
           let $textModalMonth = $("#textModalMonth");
           let $textModalYear = $("#textModalYear");

           let $saveDobBtn = $("#saveDobBtn");
           let $splitDob = text.split("-");

           $textModalDay.val($splitDob[0]);
           $textModalMonth.val($splitDob[1]);
           $textModalYear.val($splitDob[2]);

           
           $saveDobBtn.click(function () {
			   let $newDD = $textModalDay.val();
			   let $newMM = $textModalMonth.val();
			   let $newYY = $textModalYear.val();

			   let $newDOB = $newYY + "-" + $newMM + "-" + $newDD;

               $.ajax({
                   type: "POST",
                   url: "../BackEnd/profile.php",
                   dataType: "text",
                   data: {t: title, i: idNum, v : $newDOB},
                   success: function (data) {
                       let $updDOB;
                       let tJson = JSON.parse(data);
                       $updDOB = (tJson["dob"]).toString();
                       $("#dobModal").modal('hide');
                       let $splitDob = $updDOB.split("-");
                       $item.text($splitDob[2] + "-" + $splitDob[1] + "-" + $splitDob[0]);
                       $data.text($splitDob[2] + "-" + $splitDob[1] + "-" + $splitDob[0]);
                       $editTemplate.hide();
                   }
               });

           });
	}


    /*
      function for Gender manage
    */

	function addGenderBlock(title, text, idNum){
        let $node = $(TEMPLATE_GENDER);

        let $title = $node.find("#title");
        let $item = $node.find("#item");
        let $data = $node.find("#data");


        let $editTemplate = $node.find("#editTemplate");
        let $editBtn = $node.find("#editBtn");

        let $editPencil = $node.find("#editPencil");

        $title.text(title);
        $item.text(text);
        $data.text(text);

        $list.append($node);


        $editBtn.click(function(){
            let click = $(this).data('click');

            if(click){
                $editTemplate.css("display", "flex");
                $editTemplate.slideDown();
            }
            else {
                $editTemplate.css("display", "none");
                $editTemplate.slideUp();
            }
            $(this).data("click", !click);

        });

        $editPencil.click(function () {

            /*
              upload current data to modal
            */
            $modalGenLb.text(title);
            if(text === "Female"){
                $modalGenderFemale.attr("selected","selected");
            }
            if(text === "Male"){
                $modalGenderMale.attr("selected","selected");
            }
            if(text === "Other"){
               $modalGenderOther.attr("selected","selected");
            }

            $saveGenBtn.click(function () {
                let $newGenVal = $("#modalList option:selected").text();
                $.ajax({
                    type: "POST",
                    url: "../BackEnd/profile.php",
                    dataType: "text",
                    data: {t: title, i: idNum, v : $newGenVal},
                    success: function (data) {
                        let tJson = JSON.parse(data);
                        $("#editGenderModal").modal("hide");
                        let $updatedGen = (tJson["gender"]).toString();
                        $item.text($updatedGen);
                        $data.text($updatedGen);
                        $editTemplate.hide();
                    }
                });
            });
        });
    }

    /*
      function for Password manage
    */
	function addPassBlock(title, text, idNum){
        let $node = $(TEMPLATE_PANEL_PASS);

        let $title = $node.find("#title");
        let $item = $node.find("#item");
        let $data = $node.find("#data");


        let $editTemplate = $node.find("#editTemplate");
        let $editBtn = $node.find("#editBtn");

        let $editPencil = $node.find("#editPencil");

        $title.text(title);

        let $ps = convToAsterisks(text);
        $item.text($ps);
        $data.text($ps);

        $list.append($node);

        /*Show edit element on first click and hide it on second*/
        $editBtn.click(function(){
            let click = $(this).data('click');

            if(click){
                $editTemplate.css("display", "flex");
                $editTemplate.slideDown();
            }
            else {
                $editTemplate.css("display", "none");
                $editTemplate.slideUp();
            }
            $(this).data("click", !click);
        });

        $editPencil.click(function () {

            /*
              declare variable to save old and new password;
            */
			let $oldPass = $("#textOldPass");
			let $newPass = $("#textNewPass");

            /*
              hide data for old and new password
            */
			$oldPass.click(function () {
                $(this).attr("type", "password");
            });

            $newPass.click(function () {
                $(this).attr("type", "password");
            });

			let $savePassBtn = $("#savePassBtn");

            $newPass.css("pointer-events","none");

            /*
              make alert in case old password written wrong
            */
            $oldPass.blur(function () {
                let $oldPassVal = $oldPass.val();

				if($oldPassVal !== text){

                    $(".alert").show();

                    $oldPass.keyup(function () {
                        $(".alert").hide();
                        ("#passModal").removeData($oldPassVal);
                    });
				}
                else{
                    $newPass.css("pointer-events","auto");
                }
            });

            $savePassBtn.click(function () {
                let $newPassVal = $newPass.val();
                    $.ajax({
                        type: "POST",
                        url: "../BackEnd/profile.php",
                        dataType: "text",
                        data: {t: title, i: idNum, v : $newPassVal},
                        success: function (data) {
                            let tJson = JSON.parse(data);
                            let $updPass = convToAsterisks((tJson["pass"]).toString());
                            $("#passModal").modal('hide');
                            $item.text($updPass);
                            $data.text($updPass);
                            $editTemplate.hide();
                        }
                    });
            });

        });

    }

    /*
      function to upload user data from the profile.php where its taken from DB
      and displayed on the panels
    */
	function loadUserInfo(){
       $.ajax({
		   type: "POST",
		   url: "../BackEnd/profile.php",
		   dataType: "text",
		   data: {req: 1},
		   success: [ function (data) {
		   	   let t = JSON.parse(data);
               addBlock("First Name", (t["fn"]).toString(),(t["email"]).toString());
               addBlock("Last Name", (t["ln"]).toString(),(t["email"]).toString());
			   addBlock("Email", (t["email"]).toString(),(t["email"]).toString());
               addDOB_Block("Date Of Birth", (t["dd"]).toString()+"-"+(t["mm"]).toString()+"-"+(t["yy"]).toString(),(t["email"]).toString());
               addGenderBlock("Gender", (t["gender"]).toString(),(t["email"]).toString());
               addBlock("Phone number","No",(t["email"]).toString());
               addPassBlock("Password", (t["pass"]).toString(),(t["email"]).toString());
             }
           ]
	   });
       return false;
	}
	
    loadUserInfo();

    /*
      function to hide written password under asterisks
    */
    function convToAsterisks($pass){
        let $len = $pass.length;
        let $str = '*';
        for(let $i=0; $i < $len; $i++){
            $str+='*';
        }
        return $str;
    }

    /* Work with modal */
	/* Validation */
	$("#registerForm").validate({
		rules: {
            uname:{
				required: true,
                email: true,
                initEmail: true
			},
			dd:{
				required: true,
				min: 1,
				max: 31
			},
			mm:{
				required: true,
				min: 1,
				max: 12
			},
			yy:{
				required: true,
				min:1901
			},
			pass1:{
				required: true,
				initPass: true,
                minlength: 8
			},
			pass2:{
				required: true,
                equalTo: "#pass1"
			},
			fn:{
				required: true
			},
			ln:{
				required: true
			},
			tAndc:{
				required: true
			}
		},
		messages:{
            uname:{
				required: "Please, enter valid e-mail address",
                email: "Email should be in format example@host.com",
                initEmail: "Email should be in format example@host.com"
			},
			dd:{
				required: "Please, enter the day of your birth",
                min: "Number should be more then 1",
                max: "Number should be less then 31"
			},
			mm:{
                required: "Please, enter the month of your birth",
                min: "Number should be more then 1",
                max: "Number should be less then 12"
			},
			yy:{
                required: "Please, enter the year of your birth",
                min: "Number should be more then 1901",
			},
			pass1:{
                required: "Please, create the password",
                initPass: "Letters and digits; at least one capital letter and one digit",
                minlength: "Password should have at least 8 characters"
			},
			pass2:{
                required: "Please, repeat the password",
                equalTo: "Two passwords should match"
			},
			fn:{
				required: "Please, enter your first name"	
			},
			ln:{
				required: "Please, enter your last name"
			},
			tAndc:{
				required: "You should accept Terms and Conditions policy"
			}
		},
		submitHandler:function(form){
            form.submit();
		}
	});

    /*function to check e-mail*/
	$.validator.addMethod("initEmail", function (value){
        return /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
    });
	
	/*function to check password*/
    $.validator.addMethod("initPass", function(value){
        let $consist = /^[A-Za-z0-9\d=#?!@$%^&-_*]*$/.test(value);
        let $capital = /[A-Z]/.test(value);
        let $digit = /\d/.test(value);
		return $consist && $capital && $digit; 
	});
});

